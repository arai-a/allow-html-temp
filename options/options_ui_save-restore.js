function saveOptions(e) {
  e.preventDefault();

  return messenger.storage.local.set({
    debug: document.getElementById("debug").checked,
    buttonHtmlMode: document.querySelector('input[name=buttonHtmlMode]:checked').value,
    tempRemoteContent: document.getElementById("tempRemoteContent").checked,
    tempInline: document.getElementById("tempInline").checked,
    commandKey: document.getElementById("commandKey").value
  });
}

function restoreOption(id) {
  return messenger.storage.local.get(id).then((res) => {
    consoleDebug("AHT: restoreOption(id): option.id = " + id);
    let element="";
    switch(id) {
      case "commandKey":
        element = document.getElementById(id);
        element.value = res[id] || DefaultOptions[id];
        break;
      case "buttonHtmlMode":
        // radio handling
        element = document.getElementById(res[id] || DefaultOptions[id]);
        element.checked = true;
        break;
      default:
        // checkbox handling
        element = document.getElementById(id);
        if (element.type && element.type == "checkbox") {
          consoleDebug( "AHT: restoreOption(id): " + element.id + " = " + res[id] );
          if (res[id] === undefined) {
            element.checked = DefaultOptions[id];
          } else {
            element.checked = res[id];
          }
        } else {
          consoleDebug( "AHT: restoreOption(id): " + element.id + " = " + res[id] );
          element.value = res[id] || DefaultOptions[id];
        }
    }
  }, defaultError);
}

async function restoreAllOptions() {
  await restoreOption("debug");
  await restoreOption("buttonHtmlMode");
  await restoreOption("tempRemoteContent");
  await restoreOption("tempInline");
  await restoreOption("commandKey");
}

async function resetToDefault() {
  // Reset LegacyPrefs
  let window = await messenger.windows.getCurrent();
  let updateProperties = {
    msgBodyAs: "original",
    disableRemoteContent: true,
    attachmentsInline: true
  };
  await messenger.messageContentPolicy.update(window.id, updateProperties, false);
  await initLegacyPrefs();

  // Reset localstorage options
  return messenger.storage.local.remove(OptionsList).then(() => {
    restoreAllOptions();
  });
}

async function resetToRecommended() {
  // Reset LegacyPrefs
  let window = await messenger.windows.getCurrent();
  let updateProperties = {
    msgBodyAs: "sanitized",
    disableRemoteContent: true,
    attachmentsInline: false
  };
  await messenger.messageContentPolicy.update(window.id, updateProperties, false);
  await initLegacyPrefs();

  // Reset localstorage options
  return messenger.storage.local.remove(OptionsList).then(() => {
    restoreAllOptions();
  });
}

////// Handle Legacy Prefs by the Experiment API "Legacy Prefs" /////////////////////////////////
async function initLegacyPrefs() {
  consoleDebug("AHT: function initLegacyPrefs()");

  let AllowHTML_menuitem = document.getElementById("appMode_html");
  let Sanitized_menuitem = document.getElementById("appMode_sanitized");
  let AsPlaintext_menuitem = document.getElementById("appMode_plaintext");
  let AllBodyParts_menuitem = document.getElementById("appMode_allBodyParts");

  let currentPolicy = await messenger.messageContentPolicy.getCurrent();
  consoleDebug("AHT: initLegacyPrefs:", currentPolicy);

  let element = document.getElementById("allBodyParts");
  if(currentPolicy.showAllBodyPartsMenuitem) {
    if (element.classList.contains("hidden")) {
      element.classList.remove("hidden");
      consoleDebug("AHT: initLegacyPrefs: element.classList.remove");
    }
  } else {
    if (!element.classList.contains("hidden")) {
      element.classList.add("hidden");
      consoleDebug("AHT: initLegacyPrefs: element.classList.add");
    }
  }

  consoleDebug("AHT: initLegacyPrefs: set the correct radio item as checked.");

  switch (currentPolicy.msgBodyAs) {
    case "allBodyParts":
      if (AllBodyParts_menuitem) {
        AllBodyParts_menuitem.checked = true;
      }
      break;
    case "original":
      if (AllowHTML_menuitem) {
        AllowHTML_menuitem.checked = true;
      }
      break;
    case "sanitized":
      if (Sanitized_menuitem) {
        Sanitized_menuitem.checked = true;
      }
      break;
    case "plaintext":
      if (AsPlaintext_menuitem) {
        AsPlaintext_menuitem.checked = true;
      }
      break;
  }

  document.getElementById("appRemoteContent").checked = !(currentPolicy.disableRemoteContent);
  document.getElementById("allwaysInline").checked = currentPolicy.attachmentsInline;

  enableOrDisableOptionUiElements();
}

async function setPrefsMsgBodyAllowHTML() {
  consoleDebug("AHT: function setPrefsMsgBodyAllowHTML()");
  let window = await messenger.windows.getCurrent();
  let updateProperties = {
    msgBodyAs: "original"
  };
  await messenger.messageContentPolicy.update(window.id, updateProperties, false);
}

async function setPrefsMsgBodySanitized() {
  consoleDebug("AHT: function setPrefsMsgBodySanitized()");
  let window = await messenger.windows.getCurrent();
  let updateProperties = {
    msgBodyAs: "sanitized"
  };
  await messenger.messageContentPolicy.update(window.id, updateProperties, false);
}

async function setPrefsMsgBodyAsPlaintext() {
  consoleDebug("AHT: function setPrefsMsgBodyAsPlaintext()");
  let window = await messenger.windows.getCurrent();
  let updateProperties = {
    msgBodyAs: "plaintext"
  };
  await messenger.messageContentPolicy.update(window.id, updateProperties, false);
}

async function setPrefsMsgBodyAllParts() {
  consoleDebug("AHT: function setPrefsMsgBodyAllParts()");
  let window = await messenger.windows.getCurrent();
  let updateProperties = {
    msgBodyAs: "allBodyParts"
  };
  await messenger.messageContentPolicy.update(window.id, updateProperties, false);
}

async function setPrefsAppRemoteContent() {
  consoleDebug("AHT: function setPrefsAppRemoteContent()");
  let window = await messenger.windows.getCurrent();
  let updateProperties = {
    disableRemoteContent: !(document.getElementById("appRemoteContent").checked)
  };
  await messenger.messageContentPolicy.update(window.id, updateProperties, false);
  enableOrDisableOptionUiElements();
}

async function setPrefsAppAttachmentsInline() {
  consoleDebug("AHT: function setPrefsAppAttachmentsInline()");
  let window = await messenger.windows.getCurrent();
  let updateProperties = {
    attachmentsInline: document.getElementById("allwaysInline").checked
  };
  await messenger.messageContentPolicy.update(window.id, updateProperties, false);
  enableOrDisableOptionUiElements();
}
